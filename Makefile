PROG=	jexec-static
LDADD=	-ljail
MAN=	${PROG}.8
WARNS=	4

PREFIX?=	/usr/local
BINDIR=	${PREFIX}/bin
DEBUGDIR=	${PREFIX}/lib/debug
MANDIR=	${PREFIX}/man

man.md: ${PROG}.md

${PROG}.md: ${MAN} man.set-date
	${MANDOC_CMD} -Tmarkdown ${MAN} > $@

.PHONY: man.set-date
man.set-date:
	sed -i~ -e "/^\.Dd/s/.*/.Dd $$(git log -n 1 --format='%ad' --date=format:'%B %e, %Y' ${MAN})/" ${MAN}

.include <bsd.prog.mk>
