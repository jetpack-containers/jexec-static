Before do
  @objdir ||= `make -V .OBJDIR`.strip
end

Given("a persistent jail") do
  @rootfs = Dir.mktmpdir('jexec-static-test-rootfs')
  FileUtils.chmod(0755, @rootfs)
  Tempfile.create('jexec-static-test-jid') do |f|
    f.close
    system 'jail', '-q', '-J', f.path, '-c',
           "name=jexec-static-test-#{$$}",
           "path=#{@rootfs}",
           "host.hostname=jexec-static-test-#{$$}",
           "vnet=new",
           "persist" or raise "jail creation failed: #{$?}"
    jidfile=File.read(f.path)
    jidfile =~ /jid=(\d+)/ or raise "JID not found in jidfile: #{jidfile.inspect}"
    @jid = $1
  end
end

After do
  system 'jail', '-q', '-r', @jid if @jid
  FileUtils.rm_rf @rootfs if @rootfs
end

When(/^I run:\s*(.*)/) do |args|
  @command = Shellwords.split(args).map do |arg|
    case arg
    when 'jexec-static' then File.join(@objdir, 'jexec-static')
    when '$JID' then @jid
    else arg
    end
  end

  Tempfile.create 'jexec-static-test-stdout' do |fout|
    Tempfile.create 'jexec-static-test-stderr' do |ferr|
      system *@command, out: fout, err: ferr
      @command_status = $?
      fout.seek(0)
      @command_stdout = fout.read
      ferr.seek(0)
      @command_stderr = ferr.read
    end
  end
end

Given("a directory {string} in jail's root") do |path|
  assert @rootfs
  path = File.join(@rootfs, path)
  FileUtils.mkdir_p(path, verbose: true)
end

Then("the command is successful") do
  assert @command_status.success?, "Error (#{@command_status}) from command #{Shellwords.join(@command)}; stderr: #{@command_stderr.inspect}"
end

Then("the command fails") do
  assert !@command_status.success?, "Expected error from command #{Shellwords.join(@command)}; stderr: #{@command_stderr.inspect}"
end

Then("command's {stream} is {string}") do |stream, string|
  assert_equal stream, unescape_string(string)
end

Then("command's {stream} includes {string}") do |stream, string|
  assert_includes stream, string
end
