Feature: basic jexec-static features

Background:
  Given a persistent jail

Scenario: ls in an empty jail
  When I run: jexec-static $JID ls -a
  Then the command is successful
  And command's stdout is ".\n..\n"

Scenario: uid is preserved
  When I run: jexec-static $JID id -u
  Then the command is successful
  And command's stdout is "0\n"

Scenario: gid is preserved
  When I run: jexec-static $JID id -g
  Then the command is successful
  And command's stdout is "0\n"

Scenario: uid and gid override without suplementary groups
  When I run: jexec-static -u 1 -g 2 $JID id
  Then the command is successful
  And command's stdout is "uid=1 gid=2 groups=2\n"

Scenario: uid and gid override with suplementary groups
  When I run: jexec-static -u 1 -g 4,8,15,16,23,42 $JID id
  Then the command is successful
  And command's stdout is "uid=1 gid=4 groups=4,8,15,16,23,42\n"

Scenario: current directory defaults to root
  When I run: jexec-static $JID pwd
  Then the command is successful
  And command's stdout is "/\n"

Scenario: -C with nonexistent directory fails
  When I run: jexec-static -C /tmp $JID pwd
  Then the command fails
  And command's stderr includes "No such file or directory"

Scenario: -C changes current directory
  Given a directory "/tmp" in jail's root
  When I run: jexec-static -C /tmp $JID pwd
  Then the command is successful
  And command's stdout is "/tmp\n"
