require 'fileutils'
require 'json'
require 'shellwords'
require 'tempfile'
require 'tmpdir'

require 'minitest'

class MinitestWorld
  include Minitest::Assertions
  attr_accessor :assertions

  def initialize
    @assertions = 0
  end

  def unescape_string(str)
    JSON.load("\"#{str}\"")
  end
end

World { MinitestWorld.new }
