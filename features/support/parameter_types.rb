ParameterType(
  name: 'stream',
  regexp: /stdout|stderr/,
  transformer: -> (name) {
    case name
    when 'stdout' then @command_stdout
    when 'stderr' then @command_stderr
    else raise "can't happen: #{name.inspect}"
    end
  }
)
