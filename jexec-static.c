#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/param.h>
#include <sys/jail.h>
#include <jail.h>
#include <err.h>

static void usage(void);
static unsigned int parse_uint(const char *str);

int main(int argc, char **argv, char **envp)
{
	int jid = 0;
	int ch = 0;
	uid_t uid = 0;
	gid_t *gids = NULL;
	size_t ngroups = 0;
	char *bin_path = NULL;
	char *uid_opt = NULL;
	char *gid_opt = NULL;
	const char *chdir_opt = "/";

	while ((ch = getopt(argc, argv, "C:u:g:")) != -1) {
		switch(ch) {
		case 'C':
			chdir_opt = optarg;
			break;
		case 'u':
			uid_opt = optarg;
			break;
		case 'g':
			gid_opt = optarg;
			break;
		default:
			usage();
		}
	}
	argv += optind;
	argc -= optind;

	if (argc < 2)
		usage();

	if (uid_opt) {
		uid = parse_uint(uid_opt);
	}

	if (gid_opt) {
		char *cur;

		/* count groups */
		for ( cur=gid_opt ; cur ; cur = strchr(cur, ',') ) {
			ngroups++;
			if (cur)
				cur++;
		}

		/* allocate groups */
		gids = alloca(ngroups * sizeof(gid_t));

		/* parse groups */
		size_t i = 0;
		while ((cur = strsep(&gid_opt, ",")) != NULL) {
			gids[i] = parse_uint(cur);
			i++;
		}
	}

	jid = jail_getid(argv[0]);
	if (jid < 0)
		errx(1, "%s", jail_errmsg);

	if (strchr(argv[1], '/')) {
		/* absolute path */
		bin_path = argv[1];
	} else {
		/* relative path, add /rescue prefix */
		bin_path = alloca(strlen(argv[1])+9);
		if (sprintf(bin_path, "/rescue/%s", argv[1]) < 0)
			err(1, "sprintf");
	}

	int fd = open(bin_path, O_RDONLY|O_EXEC);
	if (fd < 0)
		err(1, "open(\"%s\")", bin_path);

	if (jail_attach(jid) < 0)
		err(1, "jail_attach(%d)", jid);

	if (ngroups) {
		if (setgroups(ngroups, gids) < 0)
			err(1, "setgroups");
		if (setgid(gids[0]) < 0)
			err(1, "setgid(%d)", gids[0]);
	}

	if (uid_opt) {
		if (setuid(uid) < 0)
			err(1, "setuid(%u)", uid);
	}

	if (chdir(chdir_opt) < 0)
		err(1, "chdir(\"%s\")", chdir_opt);

	fexecve(fd, argv+1, envp);
	err(1, "fexecve");
}

static void usage(void)
{
	fprintf(stderr, "usage: %s [-C PATH] [-u UID] [-g GID] JAIL COMMAND [...]\n",
			getprogname());
	exit(1);
}

static unsigned int parse_uint(const char *str) {
	if (!str || !*str) {
		errx(1, "number not provided");
	}
	char *endp;
	long val = strtol(str, &endp, 10);
	if (val<0 || *endp) {
		errx(1, "invalid number: %s", str);
	}
	return val;
}
