JEXEC-STATIC(8) - FreeBSD System Manager's Manual

# NAME

**jexec-static** - execute a static binary from host inside an existing jail

# SYNOPSIS

**jexec-static**
\[**-C**&nbsp;*path*]
\[**-u**&nbsp;*uid*]
\[**-g**&nbsp;*gid*\[,gid,...]]
*jail*
*command&nbsp;...*

# DESCRIPTION

The
**jexec-static**
utility executes
*command*
inside the
*jail*
identified by its jid or name.
Unlike the standard
jexec(8)
utility, the
*command*
is opened from the
*host*
filesystem, and then executed with
fexecve(2)
inside jail.

If the
*command*
is not a full path, the
*/rescue*
directory is searched.

The following options are available:

**-C** *path*

> Before executing
> *command*,
> change working directory to
> *path*
> within jail.
> By default,
> *command*
> is executed in jail's root directory.

**-u** *uid*

> Set user ID to
> *uid*
> (a number) before executing
> *command*.

**-g** *gid*\[,gid,...]

> Set group ID to
> *gid*
> (a number) before executing
> *command*.
> If multiple group IDs are provided, they are used as supplementary
> group IDs.

# ENVIRONMENT

Environment is not changed before executing
*command*.

# EXIT STATUS

The
**jexec-static**
utility exits
*command*'s
exit status on success, and &gt;0 if an error occurs.

# EXAMPLES

Configure network interface in a VNET jail:

	jexec-static 23 ifconfig epair0b inet 10.0.0.3 netmask 255.255.255.0

# SEE ALSO

fexecve(2),
jexec(8),
rescue(8)

# AUTHORS

Maciej Pasternacki &lt;[maciej@3ofcoins.net](mailto:maciej@3ofcoins.net)&gt;

[Project homepage](https://gitlab.com/jetpack-containers/jexec-static/)

# CAVEATS

Be aware that dynamic linking happens
*after*
execve(2).
In effect, it is not a good idea to run dynamically linked programs
with
**jexec-static**.
Even if jail filesystem includes necessary shared libraries, it
defeats the whole idea of not relying on jail contents.

For similar reasons, shell or other
'`#!`'
scripts will not run correctly.

As the name suggests, statically compiled binaries are the only type
of program that can be safely run with
**jexec-static**.

FreeBSD 12.0-ALPHA4 - November 29, 2018
