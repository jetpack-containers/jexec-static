jexec-static
============

This is a utility for FreeBSD system to execute a command from host
system in a jail. This makes it possible to adjust jail environment
without depending on its content (e.g. run `ifconfig` for a VNET jail
with Linux system root).

Build and installation
----------------------

On a FreeBSD system, `make` will build the program, and `make install`
will install it in `/usr/local/bin`.

Usage
-----

See [the manual page](./jexec-static.md)

Tests
-----

[Cucumber](https://docs.cucumber.io/) is used to run the tests. Ports
`devel/rubygem-cucumber` and `devel/rubygem-minitest` are needed to
run them:

    pkg install rubygem-cucumber rubygem-minitest

In order to create and enter jail, tests need to run as root. Simply
run `cucumber` as root (e.g. `sudo cucumber`) to run the tests.

Tests live in the [`features/`](./features) subdirectory. Scenarios
are described in the [`basic.feature`](./features/basic.feature) file,
steps are defined in the
[`step_definitions/basic.rb`](./features/step_definitions/basic.rb)
file, and the [`env/`](./features/env/) subdirectory contains
low-level plumbing.
